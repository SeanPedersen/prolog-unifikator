/*
Aufgabe 3 - Teil 2: Unifkation
Gruppe 2 Team 03
Sean Pedersen
*/

% UNIFIKATIONS-ALGORITHMUS-PRAEDIKAT:
% my_unify(INPUT: Term1, INPUT: Term2)

% Fall 1: Term1 und Term2 sind Variablen
% Erledigt Aufgabe 2 vom Unifikations-Algorithmus im Skript

my_unify(Term1, Term2) :- var(Term1), var(Term2), % Term1 und Term2 sind unbelegte Variablen
                          Term1 = Term2, !.% green cut


% Fall 2&3: Ein Term ist Variable, der Andere Funktion
% Erledigt Aufgabe 3 vom Unifikations-Algorithmus im Skript

my_unify(Term1, Term2) :- var(Term1), nonvar(Term2), % Fall 2
                          occur_check(Term1, Term2), % Occurrence Check
                          Term1 = Term2, ! ; % green cut
                          nonvar(Term1), var(Term2), % Fall 3
                          occur_check(Term2, Term1), % Occurrence Check
                          Term1 = Term2, !. % green cut

% Term ist Variable (nur durch occur_check_args/2 erreichbar, nicht über my_unify/2)
occur_check(Var, Term) :- var(Var), var(Term), % Var und Term sind unbelegte Variablen
                          Var \== Term.
% Term ist Funktion / Konstante
occur_check(Var, Term) :- var(Var), nonvar(Term), % Var ist Variable, Term ist Funktion (nötig, damit Term keine Var ist)
                          Term =.. [_Funk|Args], % Teile Funktion auf in eine Liste [Funktionskopf|Argumente]
                          occur_check_args(Var, Args). % Wenn Term eine Konstante ist, dann ist Args = [] (Listenende)
% Bottom up Rekursion
occur_check_args(_Var, []). % Rekursionsabbruch: Keine Argumente (mehr)
occur_check_args(Var, [H|RestArgs]) :- occur_check(Var, H), % Prüfe H (Argument) (wenn H auch eine mehrstellige Funktion ist, werden Args hier abgearbeitet
                                       occur_check_args(Var, RestArgs). % H (Argument) geprüft, weiter in der Rekursion mit restlichen Args


% Fall 4: Term1 und Term2 sind Funktionen.
% Erledigt Aufgabe 4 vom Unifikations-Algorithmus im Skript

my_unify(Term1, Term2) :- nonvar(Term1), nonvar(Term2), % Term1 und Term2 sind Funktionen
                         Term1 =.. [Funk1|Args1], Term2 =.. [Funk2|Args2],% Teile Funktionen auf in zwei Listen [Funktionskopf|Argumente]
                         Funk1 == Funk2, % Gleiche Funktion(sköpfe)
                         my_length(Args1, Args1Len), my_length(Args2, Args2Len), Args1Len == Args2Len, % Gleiche Anzahl an Argumenten in beiden Funktionen
                         unify_args(Args1, Args2). % Argumentliste1 unifizierbar mit Argumentliste2

% Hilfspraedikat unify_args(INPUT: Argumentliste1, INPUT: Argumentliste2) um zu pruefen, ob 2 Funktionsargument-Listen unifizierbar sind.
% Bottom Up Rekursion
unify_args([], []). % Rekursionsabbruch: keine Argumente (mehr übrig)
unify_args([H1|T1], [H2|T2]) :- my_unify(H1, H2), % Rufe my_unify/2 auf für das Paar von Funktionsargumenten der beiden Funktionen
                               unify_args(T1, T2). % Argumentenpaar wurde unifiziert, weiter in der Reukursion mit restlichen Paaren

% Hilfspraedikat my_length(INPUT:List, OUTPUT:Counter) um die Laenge einer Liste zu berechnen
% Quelle: http://www.ke.tu-darmstadt.de/lehre/archiv/ss06/prolog/teil4-listen.pdf
% Alternative Buttom Up Implementation: http://www.hsg-kl.de/faecher/inf/material/prolog/akku/index.php

% Top Down Rekursion
my_length([], 0).
my_length([_H|T], Counter) :- my_length(T, TmpCounter), Counter is TmpCounter + 1.




% Erste Implementation des OCCURENCE CHECKS ------------------------------------
% Term ist eine Variable, also muss Var ungleich Term sein.
old_occur_check(Var, Term) :- var(Var), var(Term), Var \== Term, !.
% Term ist keine Variable, ist also Funktion / Konstante.
% Konstante: keine Argumente
old_occur_check(Var, Term) :- var(Var), nonvar(Term), Term =.. [_Funk|Args], Args == [], !.
% Funktion: mit Argumenten
old_occur_check(Var, Term) :- var(Var), nonvar(Term), Term =.. [_Funk|Args], no_member(Var, Args).

% Hilfspraedikat no_member fuer occur_check. Prueft ob Var nicht in Liste der Argumente vorkommt.
no_member(_Var, []). % Rekursionsabbruch
% nächstes Element ist keine Variable, kann also eine mehrstellige Funktion sein.
no_member(Var, [H|T]) :- nonvar(H), H =.. [_Funk|Args], Args == [], Var \== H, no_member(Var, T), !. % green cut
% nächstes Element ist eine Variable kann also keine Argumente besitzen.
no_member(Var, [H|T]) :- var(H), Var \== H, no_member(Var, T), !. % green cut
% Mehrstellige Funktion mit mehrstelliger/en Funktion/en als Argument/en
% Rekursion: Suche nach Var in verschachtelten Funktionen, z.B. Var = Var, Term = f(d(s(Var)))
no_member(Var, [H|T]) :- nonvar(H), H =.. [_Funk|Args], Args \== [], Var \== H, no_member(Var, Args), no_member(Var, T).
